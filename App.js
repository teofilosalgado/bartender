import FirstStart from './views/firstStart';
import Home from './views/home';
import {createStackNavigator} from 'react-navigation';

const App = createStackNavigator({
  FirstStart: { screen: FirstStart },
  Home: { screen: Home },
},
{
  initialRouteName: 'FirstStart',
});

export default App;