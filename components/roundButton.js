import React from 'react';
import { StyleSheet, Text, View, TouchableNativeFeedback } from 'react-native';
import Theme from './../theme';

export default class RoundButton extends React.Component {
  render() {
    if (this.props.borderColor == undefined) {
      button = <View style={[styles.button, {backgroundColor: this.props.backgroundColor}]}>
        <Text style={styles.buttonText}>{this.props.title}</Text>
      </View>;
    }
    else {
      button = <View style={[styles.button, styles.borderButton, {borderColor: this.props.borderColor}]}>
        <Text style={[styles.buttonText, { color: this.props.borderColor}]}>{this.props.title}</Text>
      </View>;
    }
    return (
      <View style={[styles.base, {width:this.props.width}, this.props.style]} onPress={this.props.onPress}>
        <TouchableNativeFeedback onPress={this.props.onPress} background={TouchableNativeFeedback.Ripple(Theme.darkGray, true)}>
          {button}
        </TouchableNativeFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  base: {
    height: 50,
    borderTopLeftRadius: 25,
    borderBottomLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
  },
  button: {
    height: 50,
    width: "100%",
    borderTopLeftRadius: 25,
    borderBottomLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
    justifyContent: 'center',
    alignItems: 'center'
  },
  borderButton:{
    borderWidth: 4,
  },
  buttonText: {
    color: Theme.primaryTextColor,
    fontWeight: 'bold',
    fontSize: 16
  }
});
