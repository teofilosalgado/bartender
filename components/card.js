import React from 'react';
import { StyleSheet, Image, Text, View } from 'react-native';
import { systemWeights } from 'react-native-typography'
import Theme from '../theme'

export default class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ratingPath: [require("../assets/images/0-star.png"), 
      require("../assets/images/1-star.png"), 
      require("../assets/images/2-star.png"), 
      require("../assets/images/3-star.png"), 
      require("../assets/images/4-star.png"), 
      require("../assets/images/5-star.png")],
      difficultyPath: [require("../assets/images/0-circle.png"), 
      require("../assets/images/1-circle.png"), 
      require("../assets/images/2-circle.png"), 
      require("../assets/images/3-circle.png"), 
      require("../assets/images/4-circle.png"), 
      require("../assets/images/5-circle.png")],
    };
  }
  render() {
    return (
      <View style={styles.cardBase}>
        <View style={styles.cardImage}>

        </View>
        <View style={styles.cardTitle}>
          <Text style={[{ color: Theme.secondaryTextColor, fontSize: 22, textAlign: "center" }, systemWeights.bold]}>{this.props.title}</Text>
        </View>
        <View style={styles.mainBeverage}>
          <Image source={require("../assets/images/beverage.png")} style={{ height: "100%", width: 40 }} resizeMode="contain">

          </Image>
          <Text style={[{ color: Theme.secondaryTextColor, fontSize: 18, paddingLeft: 15, paddingRight: 15, }, systemWeights.bold]}>{this.props.beverage}</Text>
        </View>
        <View style={styles.info}>
          <View style={styles.infoBox}>
            <Text style={styles.infoBoxTitle}>Avaliações</Text>
            <Image source={this.state.ratingPath[this.props.rating]} style={{ width: "85%", height: 40 }} resizeMode="contain"></Image>
          </View>
          <View style={styles.infoBox}>
            <Text style={styles.infoBoxTitle}>Dificuldade</Text>
            <Image source={this.state.difficultyPath[this.props.difficulty]} style={{ width: "85%", height: 40 }} resizeMode="contain"></Image>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardBase: {
    backgroundColor: "#FFFFFF",
    height: 400,
    width: "100%",
    borderRadius: 5,
    marginTop:20
  },
  cardImage: {
    flex: 0.60,
    backgroundColor: "#CCCCCC",
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,
  },
  cardTitle:
  {
    padding: 20
  },
  mainBeverage: {
    paddingLeft: 15,
    paddingRight: 15,
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: "flex-start",
    alignItems: "center",
  },
  info: {
    flex: 0.2,
    paddingTop: 15,
    flexDirection: 'row',
    alignItems: "center",
    justifyContent: "flex-start"
  },
  infoBox: {
    flex: 1,
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    justifyContent: 'flex-start'
  },
  infoBoxTitle: {
    color: Theme.darkGray,
    fontSize: 16
  }
});