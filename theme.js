export default {
  primaryColor: '#6766de',
  primaryDarkColor: '#2d3cab',
  secondaryColor: '#F50057',
  secondaryDarkColor: '#BB002F',
  primaryTextColor: '#FAFAFA',
  secondaryTextColor: '#2C2C2C',
  darkGray: '#979797',
  lightGray: '#C4C4C4'
};