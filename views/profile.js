import React from 'react';
import { StyleSheet, View, Text, Image, StatusBar, ScrollView } from 'react-native';
import Card from './../components/card';
import Theme from './../theme'

export default class Profile extends React.Component {
  static navigationOptions = {
    header: null
  }
  render() {
    return (
      <View style={styles.baseContainer}>
        <Text>Hello</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  baseContainer: {
    flex: 1,
    backgroundColor: '#F5F5F5',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
});