/*
import React from 'react';
import { StyleSheet, View, Text, Image, StatusBar, ScrollView } from 'react-native';
import Card from './../components/card';
import Theme from './../theme'

export default class Home extends React.Component {
  static navigationOptions = {
    header: null
  }
  render() {
    return (
      <View style={styles.baseContainer}>
        <StatusBar backgroundColor={Theme.primaryDarkColor} barStyle="light-content"/>
        <ScrollView style={{flex:1, width:"100%", paddingLeft:20, paddingRight:20}}>
          <Card title="Capirinha" beverage="Pinga" rating="3" difficulty="1"></Card>
          <Card title="Sex on the beach" beverage="Campari" rating="4" difficulty="3"></Card>
          <View style={{height:80, width:"100%"}}></View>
        </ScrollView>
        <View style={styles.bottomNavigator}>
          <View style={styles.bottomNavigatorItem}>
            <Image source={require("../assets/images/book-off.png")} style={{ height: "50%"}} resizeMode="contain"></Image>
          </View>
          <View style={styles.bottomNavigatorItem}>
          <Image source={require("../assets/images/home-on.png")} style={{ height: "50%"}} resizeMode="contain"></Image>
          </View>
          <View style={styles.bottomNavigatorItem}>
          <Image source={require("../assets/images/profile-off.png")} style={{ height: "50%"}} resizeMode="contain"></Image>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  baseContainer: {
    flex: 1,
    backgroundColor: '#F5F5F5',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  bottomNavigator:{
    backgroundColor:"#FFFFFF",
    position:"absolute",
    bottom:0,
    height:60,
    width:"100%",
    flexDirection: 'row',
    alignItems: "center",
    justifyContent: "flex-start"
  },
  bottomNavigatorItem:{
    flex: 1,
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    justifyContent: 'flex-start'
  }
});
*/

import React from 'react';
import { Text, View } from 'react-native';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Home!</Text>
      </View>
    );
  }
}

class SettingsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings!</Text>
      </View>
    );
  }
}

export default createMaterialBottomTabNavigator({
  Home: { screen: HomeScreen },
  Settings: { screen: SettingsScreen },
}, {
  initialRouteName: 'Album',
  activeTintColor: '#f0edf6',
  inactiveTintColor: '#3e2465',
  barStyle: { backgroundColor: '#694fad' },
});