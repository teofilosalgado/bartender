import React from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import { LinearGradient} from 'expo';
import { systemWeights } from 'react-native-typography'
import RoundButton from './../components/roundButton'
import Theme from './../theme'


export default class FirstStart extends React.Component {
  static navigationOptions = {
    header: null
  }
  render() {
    return (
      <View style={styles.baseContainer}>
        <StatusBar backgroundColor={Theme.primaryDarkColor} barStyle="light-content"/>
        <LinearGradient colors={[Theme.primaryDarkColor, Theme.primaryColor]} style={styles.topContainer}>
          <Text style={[{color: Theme.primaryTextColor, fontSize: 60, marginBottom: 15}, systemWeights.bold]}>Bartender</Text>
          <Text style={[{color: Theme.primaryTextColor, fontSize: 20, textAlign: "center"}, systemWeights.light]}>As melhores receitas de drinks sempre à sua mão.</Text>
        </LinearGradient>
        <View style={styles.bottomContainer}>
          <RoundButton title="Entrar com o Google" backgroundColor={Theme.secondaryColor} width="80%" style={{marginBottom:15}} onPress={() => this.props.navigation.navigate('Home')}></RoundButton>
          <Text style={[{color: Theme.secondaryTextColor, fontSize: 20}, systemWeights.light]}>Não se preocupe, é grátis!</Text>
        </View>
        <View style={styles.eula}>
        <Text style={[{color: Theme.darkGray, fontSize: 16}, systemWeights.light]}>made with ❤ by: sigmadigital.github.io</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  baseContainer: {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  topContainer: {
    padding:20,
    alignItems: 'center', 
    alignContent:"center",
    justifyContent:'center', 
    width: '100%', 
    flex: 0.6
  },
  bottomContainer:{
    alignItems: 'center', 
    alignContent:"center",
    justifyContent:'center', 
    width: '100%', 
    flex: 0.3
  },
  eula:{
    alignItems: 'center', 
    alignContent:"center",
    justifyContent:'center', 
    width: '100%', 
    flex: 0.1
  },
  paginator:{
    width: "100%",
    flex:0.1
  },
  backgroundPrimaryColor: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%'
  },
  line: {
    flex: 3, 
    height: 1, 
    width: null, 
    backgroundColor: Theme.secondaryTextColor
  }
});
